# BB-api-u05-doe21 #
- Getting started
- Introduction - the project's aim
- Technologies
- Test and Deploy
- Scope of functionalities
- Examples of use
 -Illustrations
- Agile methods and tools
- Contributors


# Getting started #
Welcome to breaking bad endpoints.
When testing these endpoints we have two different databases.

One in a docker container and one public "physical" database.

If you are going to run tests with pytest against the docker databse you'll have to do this:

    1. in tests ==> test_main.py adn test_api.py change the import "from src.main import app" to "from ..src.main import app"
    2. In src/main.py comment the public db connection and make sure the local db conneciton is uncomment.
    3. start docker-compose up -d ==> docker-compose exec web pytest . -v

When running tests against the public database:

    1. Comment away the local databse-connection
    2. In tests ==> test_main.py make sure the app-import looks like this: "from src.main import app"


Since this is a python and Docker based project, we assume that the programs is already installed on your computer. if not, install them on your computer.


# To download app: #
 
<a href="https://gitlab.com/arashafazeli/bb-api-u05-doe21.git/"><img alt="Gitlab" src="https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white"></a>


# To access FastAPI: #
```
Docker-compose up -d --build
localhost:8008
```

# To launch tests: #
```
Docker-compose exec web pytest . -vvv
```

# Virtual enviroment recomendation: #
```
python3 -m venv env
source env/bin/activate
```

# Introduction - the project's aim #
Build an MVP (Minimum Viable Product), and focus on creating a functioning DevOps environment containing at least two pipelines, one for testing and for production.

* ### Technologies

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)   
![FastAPI](https://img.shields.io/badge/FastAPI-005571?style=for-the-badge&logo=fastapi)
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)


# Test and Deploy #
Tool to compare the python code against some of the style conventions and searching for potential bugs and errors.

- PEP8
- flake8

## Pytest

Following statuscodes and format are tested in Pytest

- status_code == 200
- status_code == 404
- status_code == 422
- response.json

# Tool to contain and deploy app: #

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)

 # Scope of functionalities: # 

- Public GitLab repository has to be initialized.
- Any requirement regarding endpoint includes not only the endpoint itself but also thorough and relevant tests.
- The API has to be ready for deployment and execution using Docker at the very - beginning.
- The repository has to contain a file which describes all of the libraries that has to been 
  installed to be able to execute both the API and the tests.
- The code has to be written accordingly to PEP8 (This does not apply to the maximum line length of 80 characters or less)


# Examples of use: #

<a href="https://github.com/arashafazeli/Breakingbad-api-doe21/releases/"><img alt="GitHub" src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white"></a>

# Illustrations: #

**Our trello bord over the sprints.**

![Trello](https://img.shields.io/badge/Trello-%23026AA7.svg?style=for-the-badge&logo=Trello&logoColor=white)
###### [https://trello.com/b/izrugjI9/breakingbadu05]



# Agile methods and tools: #

- We have worked agile with KANBAN methods.
- We used Trelloboard to plan the sprints.
- GitHub
- Gitlab
- Trelloboard
- Planing Poker
- Jam board


# Contributors: # 

- Kjell Bovin
- Alva Thunberg
- Arash Afazeli
- Ramin Dadgar
- Ludvig Ravelin

